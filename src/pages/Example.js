import React from 'react';
import { getExampleRequest } from "../store/actions/example";
import { connect } from "react-redux";

const Example = (props) => {
    const { data, getExampleRequest } = props;

    return (
        <div>
            <button onClick={() => { getExampleRequest() }}> Change Redux Store State </button>
            <h1>{data && data.chartName}</h1>
        </div>
    )
}

const mapStateToProps = (state) => ({
    data: state.example.data,
});

const mapDispatchToProps = {
    getExampleRequest
};

const Container = connect(
    mapStateToProps,
    mapDispatchToProps,
)(Example);

export default Container;
