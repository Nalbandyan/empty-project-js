import axios from 'axios';
import { stringify as qs } from 'querystringify';

const API_URL = 'https://api.coindesk.com/';

const api = axios.create({
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});

// api.interceptors.request.use(async (config) => {
//   const token = await Account.getToken();
//   if (token) {
//     config.headers.Authorization = `Bearer ${token}`;
//   }
//   return config;
// }, (error) => Promise.reject(error));

// api.interceptors.response.use((res) => res, async (error) => {
//   if (error.response && error.response.status === 401) {
//     await Account.deleteToken();
//   }
//   return Promise.reject(error);
// });

class Api {
  static url = API_URL;

  static cancelToken(key) {
    return new axios.CancelToken((c) => {
      this[`__cancel__${key}`] = c;
    });
  }

  static registration(data) {
    return api.post('/v1/users', data);
  }

  static login(data) {
    return api.post('/v1/user/login', data);
  }

  static getExamplePrice() {
    return api.get('v1/bpi/currentprice.json');
  }

  static getWithQuery(page = 1, search = '') {
    const query = qs({
      'per-page': 30,
      sort: 'ASC',
      page,
      title: search,
      sku: search,
    });
    return api.get(`/v1/product?${query}`);
  }

  static get(id) {
    return api.get(`/v1/product/${id}?expand=variants`);
  }

  static create(data) {
    return api.post('/v1/product', data);
  }

  static update(data, id) {
    return api.put(`/v1/product/${id}`, data);
  }

  static delete(id) {
    return api.put(`/v1/product/${id}`);
  }
}

export default Api;
