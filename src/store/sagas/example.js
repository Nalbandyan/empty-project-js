import {takeLatest, call, put} from 'redux-saga/effects';
import {
    EXAMPLE_REQUEST,
    EXAMPLE_SUCCESS,
    EXAMPLE_FAIL,
} from '../actions/example';
import Api from '../../Api';

function* exampleFunction(action) {
    try {
        const {data} = yield call(Api.getExamplePrice, action.payload.data);
        yield put({
            type: EXAMPLE_SUCCESS,
            payload: {data},
        });
        // if (action.payload.cb) {
        //     action.payload.cb(null, data);
        // }
    } catch (e) {
        // if (action.payload.cb) {
        //     action.payload.cb(e);
        // }
        yield put({
            type: EXAMPLE_FAIL,
            message: e.message,
        });
    }
}

export default function* watcher() {
    yield takeLatest(EXAMPLE_REQUEST, exampleFunction);
}

