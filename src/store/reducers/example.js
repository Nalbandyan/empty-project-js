import {
    EXAMPLE_REQUEST,
    EXAMPLE_SUCCESS,
    EXAMPLE_FAIL,
} from '../actions/example';

const initialState = {
    requestStatus: '',
    exampleErrors: {},
    data: {},
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case EXAMPLE_REQUEST: {
            return {
                data: state.data,
                requestStatus: 'request',
                exampleErrors: {},
            };
        }
        case EXAMPLE_SUCCESS: {
            return { 
                requestStatus: 'ok',
                data: action.payload.data,
             };
        }
        case EXAMPLE_FAIL: {
            return {
                requestStatus: 'fail',
                exampleErrors: action,
            };
        }

        default: {
            return state;
        }
    }
}
